package rs.ac.bg.etf.dp.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuRouter {
    private static MenuRouter router;
    private static String[] PATHS;
    static {
        PATHS = new String[5];
        PATHS[0] = "../resources/sbf.fxml";
        PATHS[1] = "../resources/cbf.fxml";
        PATHS[2] = "../resources/dbf.fxml";
        PATHS[3] = "../resources/simulation.fxml";
        PATHS[4] = "../resources/setreconciliationparams.fxml";
    }
    private MenuRouter() {

    }

    public static MenuRouter getRouter() {
        if (router == null) {
            router = new MenuRouter();
        }
        return router;
    }

    public void route(Stage stage, int sceneNum) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource(PATHS[sceneNum]));
            stage.setScene(new Scene(root, 1290, 750));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
