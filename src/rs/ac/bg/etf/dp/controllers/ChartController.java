package rs.ac.bg.etf.dp.controllers;

import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import rs.ac.bg.etf.dp.chart.ChartData;
import rs.ac.bg.etf.dp.set_reconciliation.SetReconciliationService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ChartController implements Initializable {
    public LineChart numOfElementsChart;
    public LineChart numOfNodesChart;
    public NumberAxis xAxis1;
    public NumberAxis yAxis1;
    public NumberAxis yAxis2;
    public NumberAxis xAxis2;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        configureNumOfElementsChart();
        configureConvergedChart();
    }

    protected void configureConvergedChart() {
        ChartData data = ChartData.getData();

        xAxis2.setAutoRanging(false);
        xAxis2.setLowerBound(0);
        xAxis2.setUpperBound(10);
        yAxis2.setAutoRanging(false);
        yAxis2.setLowerBound(0);
        yAxis2.setUpperBound(30);

        XYChart.Series seriesSbf = new XYChart.Series();
        seriesSbf.setName("SBF");
        List<Integer> numSbf = data.getNumSbf();
        seriesSbf.getData().add(new XYChart.Data(0, 0));
        for (int i = 0; i < numSbf.size(); i++) {
            seriesSbf.getData().add(new XYChart.Data(i + 1, numSbf.get(i)));
        }

        XYChart.Series seriesIbf = new XYChart.Series();
        seriesIbf.setName("IBF");
        List<Integer> numIbf = data.getNumIbf();
        seriesIbf.getData().add(new XYChart.Data(0, 0));
        for (int i = 0; i < numIbf.size(); i++) {
            seriesIbf.getData().add(new XYChart.Data(i + 1 , numIbf.get(i)));
        }

        XYChart.Series seriesDbf = new XYChart.Series();
        seriesDbf.setName("DBF");
        List<Integer> numDbf = data.getNumDbf();
        seriesDbf.getData().add(new XYChart.Data(0, 0));
        for (int i = 0; i < numDbf.size(); i++) {
            seriesDbf.getData().add(new XYChart.Data(i + 1, numDbf.get(i)));
        }

        numOfNodesChart.getData().add(seriesSbf);
        numOfNodesChart.getData().add(seriesIbf);
        numOfNodesChart.getData().add(seriesDbf);

        Node node1 = seriesSbf.getNode().lookup(".chart-series-line");
        node1.setStyle("-fx-stroke-dash-array: 10 10");
        Node node2 = seriesIbf.getNode().lookup(".chart-series-line");
        node2.setStyle("-fx-stroke-dash-array: 5 5");
    }

    protected void configureNumOfElementsChart() {
        ChartData data = ChartData.getData();

        xAxis1.setAutoRanging(false);
        xAxis1.setLowerBound(0);
        xAxis1.setUpperBound(10);
        yAxis1.setAutoRanging(false);
        yAxis1.setLowerBound(data.getStartNumberOfElements());
        yAxis1.setUpperBound(data.getMaxNumberOfElements() + 5);

        XYChart.Series seriesSbf = new XYChart.Series();
        seriesSbf.setName("SBF");
        List<Integer> dataSbf = data.getDataSbf();
        for (int i = 0; i < dataSbf.size(); i++) {
            seriesSbf.getData().add(new XYChart.Data(i, dataSbf.get(i)));
        }

        XYChart.Series seriesIbf = new XYChart.Series();
        seriesIbf.setName("IBF");
        List<Integer> dataIbf = data.getDataIbf();
        for (int i = 0; i < dataIbf.size(); i++) {
            seriesIbf.getData().add(new XYChart.Data(i , dataIbf.get(i)));
        }

        XYChart.Series seriesDbf = new XYChart.Series();
        seriesDbf.setName("DBF");
        List<Integer> dataDbf = data.getDataDbf();
        for (int i = 0; i < dataDbf.size(); i++) {
            seriesDbf.getData().add(new XYChart.Data(i, dataDbf.get(i)));
        }

        numOfElementsChart.getData().add(seriesSbf);
        numOfElementsChart.getData().add(seriesIbf);
        numOfElementsChart.getData().add(seriesDbf);

        Node node1 = seriesSbf.getNode().lookup(".chart-series-line");
        node1.setStyle("-fx-stroke-dash-array: 10 10");
        Node node2 = seriesIbf.getNode().lookup(".chart-series-line");
        node2.setStyle("-fx-stroke-dash-array: 5 5");
    }
}
