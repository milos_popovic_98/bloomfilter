package rs.ac.bg.etf.dp.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import rs.ac.bg.etf.dp.simulation.Simulation;
import rs.ac.bg.etf.dp.simulation.SimulationButton;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class SimulationController implements Initializable {

    public Button step_back;
    public Button play;
    public Button pause;
    public Button stop;
    public Button step_forward;

    public GridPane matrix;
    public TextField numOfElementsField;
    public TextField filterSizeField;
    public Button calc;
    public Label fpp;
    public Label numberOfHashFunctions;
    public BorderPane root;
    public Label operation;
    public Label element;
    public Label result;
    public Label resLabel;
    public HBox bottomHBox;

    private boolean paused;
    private boolean flag;
    private Simulation thread;
    private Button[] buttons;
    private int numOfRows;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        bottomHBox.setStyle("-fx-background-color: #0c2e3d;");
        root.setStyle("-fx-background-color: #225b73;");
        matrix.setStyle("-fx-background-color: #3296c2;");
        String[] images = new String[]{"images/step_back.PNG",
                "images/play.PNG",
                "images/pause.PNG",
                "images/stop.PNG",
                "images/step_forward.PNG"};
        Button[] buttons = new Button[]{step_back, play, pause, stop, step_forward};
        loadImages(images, buttons);
    }

    synchronized public void nextIteration() {
        try {
            while (paused && !flag) {
                wait();
            }
            if (flag) flag = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    synchronized public void insertDone() {
        try {
            wait();
            Platform.runLater(() -> {
                step_back.setDisable(true);
                operation.setText("upit");
                resLabel.setText("Rezultat:");
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    synchronized public void pause() {
        paused = true;
    }

    synchronized public void play() {
        if (thread == null) {
            operation.setText("Umetanje");
            generateMatrix();
            thread = new Simulation(this, buttons, Integer.parseInt(filterSizeField.getText()), Integer.parseInt(numOfElementsField.getText()), 1000);
            thread.start();
        } else {
            paused = false;
            notifyAll();
        }
    }

    synchronized public void stepBack() {
        if (thread != null) {
            thread.restore();
        }
    }

    synchronized public void stepForward() {
        if (thread != null) {
            flag = true;
            notifyAll();
        }
    }

    synchronized public void stop() {
        thread.interrupt();
    }

    protected void loadImages(String[] images, Button[] buttons) {
        int index = 0;
        for (String image : images) {
            File file = new File(image);
            Image img = new Image(file.toURI().toString());
            ImageView view = new ImageView(img);
            view.setFitWidth(40);
            view.setFitHeight(40);
            buttons[index].setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
            buttons[index].setGraphic(view);
            index++;
        }
    }

    public void calculate(ActionEvent actionEvent) {
        int numberOfHashFunctions = calcNumberOfHashFunctions();
        double fpp = calcFPP(numberOfHashFunctions);
        DecimalFormat df2 = new DecimalFormat("#.###");
        thread = null;
        this.numberOfHashFunctions.setText(numberOfHashFunctions + "");
        this.fpp.setText(df2.format(fpp));
    }

    protected void generateMatrix() {
        int filterSize = Integer.parseInt(filterSizeField.getText());
        numOfRows = calcFactor(filterSize);
        int numOfColumns = filterSize / numOfRows;
        if (numOfColumns < numOfRows) {
            int t = numOfRows;
            numOfRows = numOfColumns;
            numOfColumns = t;
        }
        buttons = new Button[filterSize];
        matrix.getChildren().removeAll(matrix.getChildren());
        for (int i = 0; i < numOfRows; i++) {
            for (int j = 0; j < numOfColumns; j++) {
                Button button = new Button();

                buttons[i * numOfColumns + j] = button;
                matrix.add(button, j, i);
            }
        }
    }

    protected int calcNumberOfHashFunctions() {
        int numberOfElements = Integer.parseInt(numOfElementsField.getText());
        int filterSize = Integer.parseInt(filterSizeField.getText());
        return (int) Math.ceil(Math.log(2) * filterSize / numberOfElements);
    }

    protected double calcFPP(int numberOfHashFunctions) {
        int numberOfElements = Integer.parseInt(numOfElementsField.getText());
        int filterSize = Integer.parseInt(filterSizeField.getText());
        return Math.pow(1 - Math.pow(1 - (double) 1 / filterSize, numberOfHashFunctions * numberOfElements), numberOfHashFunctions);
    }

    protected int calcFactor(int number) {
        for (int i = 1; i < number; i++) {
            if (number % i == 0) {
                int f2 = number / i;
                if (i * 2 >= f2) {
                    return i;
                }
            }
        }
        return 1;
    }

    public void routeSbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 0);
    }

    public void routeSetReconciliation(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 4);
    }

    public void routeCbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 1);
    }

    public void routeDbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 2);
    }

    public void showElement(String elem) {
        Platform.runLater(() -> {
            element.setText(elem);
        });
    }

    public void showResult(String res) {
        Platform.runLater(() -> {
            result.setText(res);
        });
    }

    public void showFinalResult(double fpp) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Rezultat simulacije");
            alert.setHeaderText(null);
            alert.setContentText("False positive: " + fpp);
            alert.initOwner(root.getScene().getWindow());
            alert.show();
        });
    }
}
