package rs.ac.bg.etf.dp.controllers;

import com.brunomnsilva.smartgraph.graph.Graph;
import com.brunomnsilva.smartgraph.graph.GraphEdgeList;
import com.brunomnsilva.smartgraph.graph.Vertex;
import com.brunomnsilva.smartgraph.graphview.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.json.JSONArray;
import org.json.JSONObject;
import rs.ac.bg.etf.dp.set_reconciliation.GraphVisualisation;
import rs.ac.bg.etf.dp.set_reconciliation.MyVertex;
import rs.ac.bg.etf.dp.set_reconciliation.SetReconciliationService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class SetReconciliationController implements Initializable {
    public Button play;
    public Button pause;
    public Button step_forward;
    public Button graphic;
    public GridPane grid;
    public Label iteration;
    public HBox hBox;
    public BorderPane root;

    private boolean started;
    private SmartGraphPanel<MyVertex, String> graphView1;
    private SmartGraphPanel<MyVertex, String> graphView2;
    private SmartGraphPanel<MyVertex, String> graphView3;
    private int iterVal;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        grid.setStyle("-fx-background-color: #142d54;");
        hBox.setStyle("-fx-background-color: #142d54;");
        String[] images = new String[]{"images/play.PNG",
                "images/pause.PNG",
                "images/step_forward.PNG",
                "images/graphic.PNG"};
        Button[] buttons = new Button[]{play, pause, step_forward, graphic};
        loadImages(images, buttons);
        SmartPlacementStrategy strategy = new SmartCircularSortedPlacementStrategy();
        SetReconciliationService service = SetReconciliationService.getService();

        MyVertex[] verticesStandard = service.getVerticesStandard();
        Graph<MyVertex, String> g1 = build_graph(verticesStandard);
        MyVertex[] verticesInvertible = service.getVerticesInvertible();
        Graph<MyVertex, String> g2 = build_graph(verticesInvertible);
        MyVertex[] verticesDistributed = service.getVerticesDistributed();
        Graph<MyVertex, String> g3 = build_graph(verticesDistributed);
        graphView1 = new SmartGraphPanel<>(g1, strategy);
        graphView2 = new SmartGraphPanel<>(g2, strategy);
        graphView3 = new SmartGraphPanel<>(g3, strategy);
        graphView1.setAutomaticLayout(true);
        graphView2.setAutomaticLayout(true);
        graphView3.setAutomaticLayout(true);
        grid.add(graphView1, 0, 2);
        grid.add(graphView2, 1, 2);
        grid.add(graphView3, 2, 2);
        Platform.runLater( () -> {
            graphView1.init();
            graphView2.init();
            graphView3.init();
            graphView1.setBackground(new Background(new BackgroundFill(Color.valueOf("#376cbf"), CornerRadii.EMPTY, Insets.EMPTY)));
            graphView2.setBackground(new Background(new BackgroundFill(Color.valueOf("#6080b3"), CornerRadii.EMPTY, Insets.EMPTY)));
            graphView3.setBackground(new Background(new BackgroundFill(Color.valueOf("#376cbf"), CornerRadii.EMPTY, Insets.EMPTY)));
            play.setDisable(false);
            pause.setDisable(false);
            step_forward.setDisable(false);
        });
    }

    protected void loadImages(String[] images, Button[] buttons) {
        int index = 0;
        for (String image : images) {
            File file = new File(image);
            Image img = new Image(file.toURI().toString());
            ImageView view = new ImageView(img);
            view.setFitWidth(40);
            view.setFitHeight(40);
            buttons[index].setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
            buttons[index].setGraphic(view);
            index++;
        }
    }

    protected Graph<MyVertex, String> build_graph(MyVertex[] vertices) {
        Graph<MyVertex, String> graph = new GraphEdgeList<>();
        for (MyVertex vertex : vertices) {
            graph.insertVertex(vertex);
        }
        for (MyVertex vertex : vertices) {
            int id = vertex.getId();
            for (int neighId : vertex.getNeighbours()) {
                if (id < neighId) {
                    graph.insertEdge(vertex, vertices[neighId], "" + id + neighId);
                }
            }
        }
        return graph;
    }

    public void play(ActionEvent actionEvent) {
        if (!started) {
            Thread thread = new Thread(new GraphVisualisation(this, graphView1, graphView2, graphView3));
            thread.start();
            started = true;
        } else {
            SetReconciliationService service = SetReconciliationService.getService();
            service.play();
        }
    }

    public void pause(ActionEvent actionEvent) {
        SetReconciliationService service = SetReconciliationService.getService();
        service.pause();
    }

    public void stepForward(ActionEvent actionEvent) {
        SetReconciliationService service = SetReconciliationService.getService();
        service.nextStep();
    }

    public void switchButtons() {
        play.setDisable(true);
        pause.setDisable(true);
        step_forward.setDisable(true);
        graphic.setDisable(false);
    }

    public void updateLabel() {
        Platform.runLater(() -> {
            iterVal++;
            iteration.setText("Iteracija: " + iterVal);
        });
    }

    public void showGraphics(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../resources/chart.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Graphic");
            stage.setScene(new Scene(root, 1000, 400));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void routeSbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 0);
    }

    public void routeSetReconciliation(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 4);
    }

    public void routeCbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 1);
    }

    public void routeDbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 2);
    }

    public void routeSim(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 3);
    }

}
