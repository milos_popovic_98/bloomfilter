package rs.ac.bg.etf.dp.controllers;

import com.google.common.hash.Hashing;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import rs.ac.bg.etf.dp.bloomfilter.Element;
import rs.ac.bg.etf.dp.bloomfilter.HashStrategy;
import rs.ac.bg.etf.dp.bloomfilter.impl.CountingBloomFilter;
import rs.ac.bg.etf.dp.bloomfilter.impl.SimpleHashStrategy;
import rs.ac.bg.etf.dp.bloomfilter.impl.StandardBloomFilter;
import rs.ac.bg.etf.dp.bloomfilter.impl.StringElement;
import rs.ac.bg.etf.dp.simulation.CountingSimulationButton;
import rs.ac.bg.etf.dp.simulation.SimulationButton;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class CountingBloomFilterController implements Initializable {
    private static int FILTER_SIZE = 32;

    public TextField updateField;
    public Label hash1;
    public Label set;
    public Label hash2;
    public TextField queryField;
    public Label queryHash1;
    public Label queryResult;
    public Label queryHash2;
    public Label deleteHash2;
    public Label deleteHash1;
    public GridPane grid;
    public BorderPane root;
    public TabPane tab;
    public ChoiceBox choiceBox;

    private CountingSimulationButton[] buttons;
    private CountingBloomFilter bloomFilter;
    private List<String> elements;
    private List<Integer> indices;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        grid.setStyle("-fx-background-color: #225b73;");
        tab.setStyle("-fx-background-color: #0c2e3d;");
        buttons = new CountingSimulationButton[FILTER_SIZE];
        for (int i = 0; i < FILTER_SIZE; i++) {
            Button button = new Button();

            buttons[i] = new CountingSimulationButton(new SimulationButton(button));
            grid.add(button, i, 0);
            int j = i + 1;
            Label label = new Label("    " + j);
            label.setTextFill(Color.WHITE);
            grid.add(label, i, 1);

            List<HashStrategy> hashStrategies = new ArrayList<>();
            hashStrategies.add(new SimpleHashStrategy(Hashing.murmur3_32()));
            hashStrategies.add(new SimpleHashStrategy(Hashing.sha256()));
            bloomFilter = new CountingBloomFilter(FILTER_SIZE, hashStrategies);
            elements = new ArrayList<>();
            indices = new ArrayList<>();
        }
    }

    public void update(ActionEvent actionEvent) {
        String s = updateField.getText();
        Element e = new StringElement(s);
        elements.add(s);

        List<Integer> indices = bloomFilter.update(e);
        int index1 = indices.get(0);
        int index2 = indices.get(1);

        hash1.setText("" + (index1 + 1));
        hash2.setText("" + (index2 + 1));
        buttons[index1].fill();
        buttons[index2].fill();

        set.setText(elements.toString());
        choiceBox.getItems().add(s);
    }

    public void query(ActionEvent actionEvent) {
        for (Integer index : indices) {
            buttons[index].unbold();
        }
        String s = queryField.getText();
        Element e = new StringElement(s);

        indices = bloomFilter.getHash(e);
        int index1 = indices.get(0);
        int index2 = indices.get(1);
        buttons[index1].bold();
        buttons[index2].bold();
        queryHash1.setText("" + (index1 + 1));
        queryHash2.setText("" + (index2 + 1));

        if (bloomFilter.query(e)) {
            if (exists(s)) {
                queryResult.setText("Element jeste u skupu.");
            } else {
                queryResult.setText("False positive.");
            }
        } else {
            queryResult.setText("Element nije u skupu.");
        }
    }

    protected boolean exists(String s) {
        for (String elem : elements) {
            if (elem.equals(s)) {
                return true;
            }
        }
        return false;
    }

    public void delete(ActionEvent actionEvent) {
        String s = (String) choiceBox.getValue();;
        Element e = new StringElement(s);

        indices = bloomFilter.getHash(e);
        int index1 = indices.get(0);
        int index2 = indices.get(1);
        buttons[index1].erase();
        buttons[index2].erase();

        bloomFilter.delete(e);
        removeFromSet(s);
        set.setText(elements.toString());
        updateField.setText("");
        hash1.setText("");
        hash2.setText("");
    }

    protected void removeFromSet(String s) {
        for (int i = 0; i < elements.size(); i++) {
            if (s.equals(elements.get(i))) {
                elements.remove(i);
                break;
            }
        }
    }

    public void select(ActionEvent actionEvent) {
        for (Integer index : indices) {
            buttons[index].unbold();
        }
        String s = (String) choiceBox.getValue();
        Element e = new StringElement(s);

        indices = bloomFilter.getHash(e);
        int index1 = indices.get(0);
        int index2 = indices.get(1);
        buttons[index1].bold();
        buttons[index2].bold();
        deleteHash1.setText("" + (index1 + 1));
        deleteHash2.setText("" + (index2 + 1));
    }

    public void routeSimulation(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 3);
    }

    public void routeSetReconciliation(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 4);
    }

    public void routeSbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 0);
    }

    public void routeDbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 2);
    }

}
