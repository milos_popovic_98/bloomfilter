package rs.ac.bg.etf.dp.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import rs.ac.bg.etf.dp.bloomfilter.Element;
import rs.ac.bg.etf.dp.bloomfilter.impl.StandardBloomFilter;
import rs.ac.bg.etf.dp.bloomfilter.impl.StringElement;
import rs.ac.bg.etf.dp.simulation.SimulationButton;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class StandardBloomFilterController implements Initializable {
    private static int FILTER_SIZE = 32;
    public GridPane grid;
    public Label set;

    public Label hash1;
    public Label hash2;
    public TextField updateField;

    public TextField queryField;
    public Label queryHash1;
    public Label queryHash2;
    public Label queryResult;
    public BorderPane root;
    public TabPane tab;


    private SimulationButton[] buttons;
    private StandardBloomFilter bloomFilter;
    private List<String> elements;
    private List<Integer> indices;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        grid.setStyle("-fx-background-color: #225b73;");
        tab.setStyle("-fx-background-color: #0c2e3d;");
        buttons = new SimulationButton[FILTER_SIZE];
        for (int i = 0; i < FILTER_SIZE; i++) {
            Button button = new Button();

            buttons[i] = new SimulationButton(button);
            grid.add(button, i, 0);
            int j = i + 1;
            Label label = new Label("    " + j);
            label.setTextFill(Color.WHITE);
            grid.add(label, i, 1);
        }
        bloomFilter = new StandardBloomFilter(0, FILTER_SIZE, 2);
        elements = new ArrayList<>();
        indices = new ArrayList<>();
    }

    public void query(ActionEvent actionEvent) {
        for (Integer index : indices) {
            buttons[index].unbold();
        }
        String s = queryField.getText();
        Element e = new StringElement(s);

        indices = bloomFilter.getHash(e);
        int index1 = indices.get(0);
        int index2 = indices.get(1);
        buttons[index1].bold();
        buttons[index2].bold();
        queryHash1.setText("" + (index1 + 1));
        queryHash2.setText("" + (index2 + 1));

        if (bloomFilter.query(e)) {
            if (exists(s)) {
                queryResult.setText("Element jeste u skupu.");
            } else {
                queryResult.setText("False positive.");
            }
        } else {
            queryResult.setText("Element nije u skupu.");
        }
    }

    protected boolean exists(String s) {
        for (String elem : elements) {
            if (elem.equals(s)) {
                return true;
            }
        }
        return false;
    }

    public void update(ActionEvent actionEvent) {
        String s = updateField.getText();
        Element e = new StringElement(s);
        elements.add(s);

        List<Integer> indices = bloomFilter.update(e);
        int index1 = indices.get(0);
        int index2 = indices.get(1);

        hash1.setText("" + (index1 + 1));
        hash2.setText("" + (index2 + 1));
        buttons[index1].fill();
        buttons[index2].fill();

        set.setText(elements.toString());
    }

    public void routeSimulation(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 3);
    }

    public void routeSetReconciliation(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 4);
    }

    public void routeCbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 1);
    }

    public void routeDbf(ActionEvent actionEvent) {
        Stage stage = (Stage) root.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 2);
    }
}
