package rs.ac.bg.etf.dp.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import rs.ac.bg.etf.dp.set_reconciliation.SetReconciliationService;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SetReconciliationParamsController implements Initializable {
    public TextField targetNumber;
    public TextField initialNumber;
    public TextField fpp;
    public GridPane grid;
    public BorderPane border;


    public void apply(ActionEvent actionEvent) {
        try {
            int startNumberOfElements = Integer.parseInt(initialNumber.getText());
            int maxNumberOfElements = Integer.parseInt(targetNumber.getText());
            double falsePositiveRate = Double.parseDouble(fpp.getText());
            SetReconciliationService service = SetReconciliationService.getService();
            service.init(startNumberOfElements, maxNumberOfElements, falsePositiveRate);
            Parent root = FXMLLoader.load(getClass().getResource("../resources/setreconciliation.fxml"));
            Stage stage = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
            stage.setScene(new Scene(root, 1290, 750));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        border.setStyle("-fx-background-color: #103163;");
        grid.setStyle("-fx-background-color: #2c5795;");
    }

    public void routeSimulation(ActionEvent actionEvent) {
        Stage stage = (Stage) border.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 3);
    }

    public void routeSbf(ActionEvent actionEvent) {
        Stage stage = (Stage) border.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 0);
    }

    public void routeCbf(ActionEvent actionEvent) {
        Stage stage = (Stage) border.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 1);
    }

    public void routeDbf(ActionEvent actionEvent) {
        Stage stage = (Stage) border.getScene().getWindow();
        MenuRouter.getRouter().route(stage, 2);
    }
}
