package rs.ac.bg.etf.dp.chart;

import rs.ac.bg.etf.dp.set_reconciliation.SetReconciliationService;

import java.util.ArrayList;
import java.util.List;

public class ChartDataConverged {
    private static ChartDataConverged data;

    private List<Integer> dataSbf;
    private List<Integer> dataIbf;
    private List<Integer> dataDbf;

    private int startNumberOfElements;
    private int maxNumberOfElements;

    private ChartDataConverged() {
        dataSbf = new ArrayList<>();
        dataIbf = new ArrayList<>();
        dataDbf = new ArrayList<>();
    }

    public static ChartDataConverged getData() {
        if (data == null) {
            data = new ChartDataConverged();
        }
        return data;
    }

    public void addItem(int type, int val) {
        switch (type) {
            case 0:
                dataSbf.add(val);
                break;
            case 1:
                dataIbf.add(val);
                break;
            case 2:
                dataDbf.add(val);
                break;
        }
    }

    public List<Integer> getDataSbf() {
        return dataSbf;
    }

    public void setDataSbf(List<Integer> dataSbf) {
        this.dataSbf = dataSbf;
    }

    public List<Integer> getDataIbf() {
        return dataIbf;
    }

    public void setDataIbf(List<Integer> dataIbf) {
        this.dataIbf = dataIbf;
    }

    public List<Integer> getDataDbf() {
        return dataDbf;
    }

    public int getStartNumberOfElements() {
        return startNumberOfElements;
    }

    public void setStartNumberOfElements(int startNumberOfElements) {
        this.startNumberOfElements = startNumberOfElements;
    }

    public int getMaxNumberOfElements() {
        return maxNumberOfElements;
    }

    public void setMaxNumberOfElements(int maxNumberOfElements) {
        this.maxNumberOfElements = maxNumberOfElements;
    }

    public void setDataDbf(List<Integer> dataDbf) {
        this.dataDbf = dataDbf;
    }
}
