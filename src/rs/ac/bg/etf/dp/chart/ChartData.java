package rs.ac.bg.etf.dp.chart;

import rs.ac.bg.etf.dp.set_reconciliation.SetReconciliationService;

import java.util.ArrayList;
import java.util.List;

public class ChartData {
    private static ChartData data;

    private List<Integer> dataSbf;
    private List<Integer> dataIbf;
    private List<Integer> dataDbf;
    private List<Integer> numSbf;
    private List<Integer> numIbf;
    private List<Integer> numDbf;

    private int startNumberOfElements;
    private int maxNumberOfElements;

    private ChartData() {
        dataSbf = new ArrayList<>();
        dataIbf = new ArrayList<>();
        dataDbf = new ArrayList<>();

        numSbf = new ArrayList<>();
        numIbf = new ArrayList<>();
        numDbf = new ArrayList<>();
    }

    public static ChartData getData() {
        if (data == null) {
            data = new ChartData();
        }
        return data;
    }

    public void addItem(int type, int val) {
        val /= SetReconciliationService.NUM_OF_NODES;
        switch (type) {
            case 0:
                dataSbf.add(val);
                break;
            case 1:
                dataIbf.add(val);
                break;
            case 2:
                dataDbf.add(val);
                break;
        }
    }

    public void addNum(int type, int val) {
        switch (type) {
            case 0:
                numSbf.add(val);
                break;
            case 1:
                numIbf.add(val);
                break;
            case 2:
                numDbf.add(val);
                break;
        }
    }


    public List<Integer> getDataSbf() {
        return dataSbf;
    }

    public List<Integer> getDataIbf() {
        return dataIbf;
    }

    public List<Integer> getDataDbf() {
        return dataDbf;
    }

    public List<Integer> getNumSbf() {
        return numSbf;
    }

    public List<Integer> getNumIbf() {
        return numIbf;
    }

    public List<Integer> getNumDbf() {
        return numDbf;
    }

    public int getStartNumberOfElements() {
        return startNumberOfElements;
    }

    public void setStartNumberOfElements(int startNumberOfElements) {
        this.startNumberOfElements = startNumberOfElements;
    }

    public int getMaxNumberOfElements() {
        return maxNumberOfElements;
    }

    public void setMaxNumberOfElements(int maxNumberOfElements) {
        this.maxNumberOfElements = maxNumberOfElements;
    }

    public void clearData() {
        dataSbf.clear();
        dataIbf.clear();
        dataDbf.clear();

        numSbf.clear();
        numIbf.clear();
        numDbf.clear();
    }
}
