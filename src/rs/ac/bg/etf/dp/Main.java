package rs.ac.bg.etf.dp;

import com.brunomnsilva.smartgraph.graph.Graph;
import com.brunomnsilva.smartgraph.graph.GraphEdgeList;
import com.brunomnsilva.smartgraph.graph.Vertex;
import com.brunomnsilva.smartgraph.graphview.SmartCircularSortedPlacementStrategy;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;
import com.brunomnsilva.smartgraph.graphview.SmartPlacementStrategy;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import rs.ac.bg.etf.dp.controllers.SetReconciliationController;
import rs.ac.bg.etf.dp.set_reconciliation.MyVertex;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("resources/sbf.fxml"));
        /*Graph<MyVertex, String> g = build_sample_graph();
        System.out.println(g);

        SmartPlacementStrategy strategy = new SmartCircularSortedPlacementStrategy();
        SmartGraphPanel<MyVertex, String> graphView = new SmartGraphPanel<>(g, strategy);
*/
        //new SetReconciliationController().generate(200, 500, 0.2);
        primaryStage.setTitle("Bloom filter");
        primaryStage.setScene(new Scene(root, 1290, 750));
        primaryStage.show();

  /*      graphView.init();
        for (Vertex<MyVertex> vertex : g.vertices()) {
            graphView.getStylableVertex(vertex).setStyle(("-fx-fill:" + vertex.element().updateState(500)));
        }*/
    }

   /* private Graph<MyVertex, String> build_sample_graph() {
        Graph<MyVertex, String> g = new GraphEdgeList<>();
        MyVertex v1 = new MyVertex("A", 500, 1000);
        MyVertex v2 = new MyVertex("B", 500, 1000);
        MyVertex v3 = new MyVertex("C", 500, 1000);
        MyVertex v4 = new MyVertex("D", 500, 1000);
        MyVertex v5 = new MyVertex("E", 500, 1000);
        MyVertex v6 = new MyVertex("F", 500, 1000);
        MyVertex v7 = new MyVertex("G", 500, 1000);

        g.insertVertex(v1);
        g.insertVertex(v2);
        g.insertVertex(v3);
        g.insertVertex(v4);
        g.insertVertex(v5);
        g.insertVertex(v6);
        g.insertVertex(v7);

        g.insertEdge(v1, v2, "1");
        g.insertEdge(v2, v3, "2");
        g.insertEdge(v3, v4, "3");
        g.insertEdge(v4, v5, "4");
        g.insertEdge(v5, v6, "5");
        g.insertEdge(v6, v7, "6");

        return g;
    }*/


    public static void main(String[] args) {
        launch(args);
    }
}
