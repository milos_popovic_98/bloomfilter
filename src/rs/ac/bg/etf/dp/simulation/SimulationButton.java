package rs.ac.bg.etf.dp.simulation;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class SimulationButton {
    private static String[] states = new String[]{"#ffffff", "#00ff00", "#ff0000"};

    Button button;
    int state;

    public SimulationButton() {
        this(new Button());
    }
    public SimulationButton(Button button) {
        button.setPrefHeight(50);
        button.setPrefWidth(50);
        button.setStyle("-fx-border-color: #000000; -fx-background-color: #ffffff");

        this.button = button;
    }

    public void fill() {
        state++;
        if (state <= 2) {
            button.setStyle("-fx-border-color: #000000; -fx-background-color: " + states[state]);;
        }
    }

    public void bold() {
        button.setStyle("-fx-border-color: #000000; -fx-border-width: 3px; -fx-background-color: " + states[state]);;
    }

    public void unbold() {
        button.setStyle("-fx-border-color: #000000; -fx-background-color: " + states[state]);;
    }
    public void erase() {
        state--;
        if (state < 2) {
            button.setStyle("-fx-border-color: #000000; -fx-background-color: " + states[state]);;
        }
    }

    public int getState() {
        return state;
    }
}
