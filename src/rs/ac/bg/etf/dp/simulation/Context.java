package rs.ac.bg.etf.dp.simulation;

import rs.ac.bg.etf.dp.bloomfilter.BloomFilter;

import java.util.List;

public class Context {
    private List<Integer> state;
    private BloomFilter filter;
    public Context(List<Integer> state, BloomFilter filter) {
        this.state = state;
        this.filter = filter;
    }

    public List<Integer> getState() {
        return state;
    }
    public BloomFilter getFilter() {
        return filter;
    }

}
