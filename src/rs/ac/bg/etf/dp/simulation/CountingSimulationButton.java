package rs.ac.bg.etf.dp.simulation;

public class CountingSimulationButton extends SimulationButton{
    private SimulationButton simulationButton;

    public CountingSimulationButton(SimulationButton button) {
        this.simulationButton = button;
        button.button.setText(0 + "");
    }

    @Override
    public void fill() {
        simulationButton.fill();
        simulationButton.button.setText(simulationButton.state + "");
    }

    @Override
    public void erase() {
        simulationButton.erase();
        simulationButton.button.setText(simulationButton.state + "");
    }

    @Override
    public void bold() {
        simulationButton.bold();
    }

    @Override
    public void unbold() {
        simulationButton.unbold();
    }
}
