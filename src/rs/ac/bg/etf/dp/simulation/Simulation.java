package rs.ac.bg.etf.dp.simulation;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import rs.ac.bg.etf.dp.bloomfilter.BloomFilter;
import rs.ac.bg.etf.dp.bloomfilter.Element;
import rs.ac.bg.etf.dp.bloomfilter.impl.StandardBloomFilter;
import rs.ac.bg.etf.dp.bloomfilter.impl.StringElement;
import rs.ac.bg.etf.dp.controllers.SimulationController;
import rs.ac.bg.etf.dp.db.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class Simulation extends Thread {
    private int SET_SIZE = 5000;
    private BloomFilter bloomFilter;
    private List<SimulationButton> buttons;
    private int setSize;
    private int numberOfElementsToInsert;
    private int numberOfIterations;
    private int currentIteration;
    private List<Integer> allElements;
    private List<Integer> elementsToInsert;
    private Stack<Context> contextStack;
    private SimulationController controller;

    public Simulation(SimulationController controller, Button[] buttons, int setSize, int numberOfElementsToInsert, int numberOfIterations) {
        this.controller = controller;
        this.bloomFilter = new StandardBloomFilter(numberOfElementsToInsert, setSize, 0);
        this.setSize = numberOfElementsToInsert;
        this.numberOfIterations = numberOfIterations;
        this.numberOfElementsToInsert = numberOfElementsToInsert;

        this.buttons = new ArrayList<>();
        for (Button button : buttons) {
            this.buttons.add(new SimulationButton(button));
        }

        allElements = new ArrayList<>();
        for (int i = 1; i <= SET_SIZE; i++) {
            allElements.add(i);
        }
        Collections.shuffle(allElements);
        elementsToInsert = new ArrayList<>(allElements.subList(0, numberOfElementsToInsert));
        Collections.shuffle(allElements);

        contextStack = new Stack<>();
    }

    @Override
    public void run() {
        try {
            while (currentIteration < numberOfElementsToInsert) {
                Element element = getElement(elementsToInsert.get(currentIteration));
                BloomFilter copy = bloomFilter.clone();
                List<Integer> indices = bloomFilter.update(element);
                for (int index : indices) {
                    buttons.get(index).fill();
                }
                save(indices, copy);
                currentIteration++;
                controller.showElement(new String(element.getBody()));
                Thread.sleep(500);
                controller.nextIteration();
            }
            controller.insertDone();
            currentIteration = 0;
            contextStack.removeAllElements();
            int falsePositiveNumber = 0;
            while (currentIteration < numberOfIterations) {
                int id = allElements.get(currentIteration);
                Element element = getElement(id);
                boolean result = bloomFilter.query(element);
                String res;
                if (result) {
                    if (falsePositive(id)) {
                        falsePositiveNumber++;
                        res = "false positive";
                        System.out.println("Element with id " + id + " - false positive.");
                    } else {
                        res = "element jeste u skupu";
                        //System.out.println("Element with id " + id + " is in the set!");
                    }
                } else {
                    res = "element nije u skupu";
                    //System.out.println("Element with id " + id + " is not in the set");
                }
                controller.showElement(new String(element.getBody()));
                controller.showResult(res);
                //Thread.sleep(500);
                controller.nextIteration();
                currentIteration++;
            }
            System.out.println("False positive number:" + falsePositiveNumber);
            controller.showFinalResult((double)falsePositiveNumber / numberOfIterations);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected boolean falsePositive(int id) {
        for (int v : elementsToInsert) {
            if (v == id)
                return false;
        }
        return true;
    }

    protected void save(List<Integer> indices, BloomFilter filter) {
        contextStack.add(new Context(indices, filter));
    }

    public void restore() {
        Context context = contextStack.pop();
        List<Integer> indices = context.getState();
        for (int index : indices) {
            buttons.get(index).erase();
        }
        bloomFilter = context.getFilter();
        currentIteration--;
    }

    private Element getElement(int id) {
        System.out.println("Element id: " + id);
        Connection connection = DB.getInstance().getConnection();
        String query = "SELECT movie_title FROM Film WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new StringElement(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
