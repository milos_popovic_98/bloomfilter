package rs.ac.bg.etf.dp.set_reconciliation;

import org.json.JSONArray;
import org.json.JSONObject;
import rs.ac.bg.etf.dp.chart.ChartData;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SetReconciliationService {
    public static int NUM_OF_NODES = 25;
    private static int NUM_OF_NEIGHBOURS = 5;
    private static List<Integer> IDS;
    private static String[] FILES;

    private static SetReconciliationService service;

    static {
        IDS = new ArrayList<>();
        for (int i = 0; i < NUM_OF_NODES; i++) {
            IDS.add(i);
        }
        FILES = new String[]{
                "C:\\Users\\popov\\Desktop\\DiplomskiRad\\BloomFilter1\\vertices_standard.json",
                "C:\\Users\\popov\\Desktop\\DiplomskiRad\\BloomFilter1\\vertices_invertible.json",
                "C:\\Users\\popov\\Desktop\\DiplomskiRad\\BloomFilter1\\vertices_distributed.json"};
    }

    private MyVertex[] verticesStandard;
    private MyVertex[] verticesInvertible;
    private MyVertex[] verticesDistributed;

    private boolean paused;
    private boolean step_forward;

    private SetReconciliationService() {
    }

    public static SetReconciliationService getService() {
        if (service == null) {
            service = new SetReconciliationService();
        }
        return service;
    }

    public void init(int startNumberOfElements, int maxNumberOfElements, double falsePositiveRate) {
        verticesStandard = new MyVertex[NUM_OF_NODES];
        generate(startNumberOfElements, maxNumberOfElements, falsePositiveRate, verticesStandard, 0);

        verticesInvertible = new MyVertex[NUM_OF_NODES];
        generate(startNumberOfElements, maxNumberOfElements, falsePositiveRate, verticesInvertible, 1);

        verticesDistributed = new MyVertex[NUM_OF_NODES];
        generate(startNumberOfElements, maxNumberOfElements, falsePositiveRate, verticesDistributed, 2);

        ChartData data = ChartData.getData();
        data.clearData();
        data.addItem(0, startNumberOfElements * NUM_OF_NODES);
        data.addItem(1, startNumberOfElements * NUM_OF_NODES);
        data.addItem(2, startNumberOfElements * NUM_OF_NODES);
        data.setStartNumberOfElements(startNumberOfElements);
        data.setMaxNumberOfElements(maxNumberOfElements);
    }

    /*private void copyTopology(MyVertex[] source, MyVertex[] destination) {
        int counter = 0;
        for (MyVertex vertex : source) {
            destination[counter].setNeighbours(vertex.getNeighbours());
        }
    }*/

    protected void generate(int startNumberOfElements, int maxNumberOfElements, double fpp, MyVertex[] vertices, int type) {
        JSONObject content = new JSONObject();
        content.put("initial", startNumberOfElements);
        content.put("target", maxNumberOfElements);
        content.put("fpp", fpp);
        JSONArray nodes = new JSONArray();
        for (int i = 0; i < NUM_OF_NODES; i++) {
            vertices[i] = new MyVertex(i, startNumberOfElements, maxNumberOfElements, fpp, type);
        }
        for (int i = 0; i < NUM_OF_NODES; i++) {
            addNeighbours(vertices[i], vertices);

            JSONObject node = new JSONObject();
            node.put("id", i);
            node.put("neighbours", vertices[i].getNeighbours());
            nodes.put(node);
        }
        content.put("nodes", nodes);
        writeToFile(content, type);
    }

    protected void writeToFile(JSONObject content, int type) {
        File file = new File(FILES[type]);
        try (FileWriter writer = new FileWriter(file)) {
            writer.write(content.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void addNeighbours(MyVertex vertex, MyVertex[] vertices) {
        Collections.shuffle(IDS);
        int vertexId = vertex.getId();
        int counter = vertex.getNumOfNeighs();
        for (Integer neighId : IDS) {
            if (counter >= NUM_OF_NEIGHBOURS) break;
            if (neighId <= vertexId) continue;
            vertex.addNeighbour(neighId);
            vertices[neighId].addNeighbour(vertexId);
            counter++;
        }
    }

    public MyVertex[] getVerticesStandard() {
        return verticesStandard;
    }

    public MyVertex[] getVerticesInvertible() {
        return verticesInvertible;
    }

    public MyVertex[] getVerticesDistributed() {
        return verticesDistributed;
    }

    synchronized public void nextIteration() {
        try {
            while (paused && !step_forward) {
                wait();
            }
            if (step_forward) step_forward = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    synchronized public void pause() {
        paused = true;
    }

    synchronized public void nextStep() {
        step_forward = true;
        notifyAll();
    }

    synchronized public void play() {
        paused = false;
        notifyAll();
    }
}
