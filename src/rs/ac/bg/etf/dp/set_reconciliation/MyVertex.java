package rs.ac.bg.etf.dp.set_reconciliation;

import com.brunomnsilva.smartgraph.graphview.SmartLabelSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyVertex implements Cloneable{
    private static String[] COLORS = new String[]{"#FFFFCC", "#FFFF99", "#FFFF66", "#FFFF33", "#FFFF00",
            "#CCCC00", "#999900", "#666600", "#333300"};
    private static int NUM_OF_COLORS = 8;

    private enum Type {STANDARD, INVERTIBLE, DISTRIBUTED}

    private int id;
    private Type type;
    private double fpp;
    private int state;
    private int startNumOfElements;
    private int maxNumOfElements;
    private int step;
    private List<Integer> neighbours;

    public MyVertex(int id, int startNumOfElements, int maxNumberOfElements, double fpp, int type) {
        this.id = id;
        state = 0;
        this.startNumOfElements = startNumOfElements;
        this.maxNumOfElements = maxNumberOfElements;
        step = (maxNumberOfElements - startNumOfElements) / NUM_OF_COLORS;
        this.fpp = fpp;
        this.type = getType(type);
        this.neighbours = new ArrayList<>();
    }

    public String updateState(int numOfElements) {
        while (numOfElements >= (startNumOfElements + (state + 1) * step) && state < NUM_OF_COLORS) {
            state++;
        }
        //System.out.println(state);
        return COLORS[state];
    }

    public String getInitState() {
        return COLORS[0];
    }

    @Override
    public String toString() {
        return "";
    }

    protected Type getType(int type) {
        switch (type) {
            case 0: return Type.STANDARD;
            case 1: return Type.INVERTIBLE;
            case 2: return Type.DISTRIBUTED;
            default: return null;
        }
    }

    public void addNeighbour(int neighId) {
        neighbours.add(neighId);
    }

    public int getNumOfNeighs() {
        return neighbours.size();
    }

    public int getId() {
        return id;
    }

    public List<Integer> getNeighbours() {
        Collections.sort(neighbours);
        Collections.reverse(neighbours);
        return neighbours;
    }
}
