package rs.ac.bg.etf.dp.set_reconciliation;

import com.brunomnsilva.smartgraph.graph.Graph;
import com.brunomnsilva.smartgraph.graph.GraphEdgeList;
import com.brunomnsilva.smartgraph.graph.Vertex;
import com.brunomnsilva.smartgraph.graphview.SmartCircularSortedPlacementStrategy;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;
import com.brunomnsilva.smartgraph.graphview.SmartPlacementStrategy;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import rs.ac.bg.etf.dp.GoClient;
import rs.ac.bg.etf.dp.chart.ChartData;
import rs.ac.bg.etf.dp.controllers.SetReconciliationController;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class GraphVisualisation implements Runnable {
    private static int NUM_OF_ITERATIONS = 10;
    private static String FILE_NAME = "C:\\Users\\popov\\Desktop\\DiplomskiRad\\BloomFilter1\\result.txt";

    private final SmartGraphPanel<MyVertex, String> graphView1;
    private final SmartGraphPanel<MyVertex, String> graphView2;
    private final SmartGraphPanel<MyVertex, String> graphView3;
    private final SetReconciliationController controller;

    public GraphVisualisation(SetReconciliationController controller,
                              SmartGraphPanel<MyVertex, String> graphView1,
                              SmartGraphPanel<MyVertex, String> graphView2,
                              SmartGraphPanel<MyVertex, String> graphView3) {
        this.graphView1 = graphView1;
        this.graphView2 = graphView2;
        this.graphView3 = graphView3;
        this.controller = controller;
    }

    @Override
    public void run() {
        SetReconciliationService service = SetReconciliationService.getService();
        MyVertex[] vertices1 = service.getVerticesStandard();
        MyVertex[] vertices2 = service.getVerticesInvertible();
        MyVertex[] vertices3 = service.getVerticesDistributed();
        initialize(graphView1, vertices1);
        initialize(graphView2, vertices2);
        initialize(graphView3, vertices3);
        GoClient goClient = GoClient.getClient();
        goClient.simulate();

        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME))) {
            for (int i = 0; i < NUM_OF_ITERATIONS; i++) {
                controller.updateLabel();
                String line1 = reader.readLine();
                String line2 = reader.readLine();
                String line3 = reader.readLine();
                updateState(graphView1, vertices1, line1, 0);
                updateState(graphView2, vertices2, line2, 1);
                updateState(graphView3, vertices3, line3, 2);
                Thread.sleep(100 * vertices1.length + 100);
                service.nextIteration();
            }
            controller.switchButtons();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void initialize(SmartGraphPanel<MyVertex, String> graphView, MyVertex[] vertices) {
        Platform.runLater(() -> {
            for (MyVertex vertex : vertices) {
                graphView.getStylableVertex(vertex).setStyle("-fx-fill:" + vertex.getInitState());
            }
        });
    }

    protected void updateState(SmartGraphPanel<MyVertex, String> graphView, MyVertex[] vertices, String line, int type) throws InterruptedException {
        List<Integer> nextState = getNextState(line);
        ChartData chartData = ChartData.getData();
        int targetNum = chartData.getMaxNumberOfElements();
        int sum = 0;
        int count = 0;
        for (int val : nextState) {
            sum += val;
            if (val == targetNum)
                count++;
        }
        chartData.addItem(type, sum);
        chartData.addNum(type, count);
        new Thread(() -> {
            for (int i = 0; i < vertices.length; i++) {
                MyVertex vertex = vertices[i];
                int index = i;
                Platform.runLater(() -> {
                    graphView.getStylableVertex(vertex).setStyle("-fx-fill:" + vertex.updateState(nextState.get(index)));
                });
                try {
                    Thread.sleep(80);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    protected List<Integer> getNextState(String line) {
        List<Integer> state = new ArrayList<>();
        line.replaceFirst(" ", "");
        StringTokenizer st = new StringTokenizer(line, " ");
        while (st.hasMoreTokens()) {
            state.add(Integer.parseInt(st.nextToken()));
        }
        return state;
    }


}
