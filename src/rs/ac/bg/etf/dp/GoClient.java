package rs.ac.bg.etf.dp;

import com.sun.jna.*;

public class GoClient {
    private static String FILE_PATH = "C:\\Users\\popov\\Desktop\\DiplomskiRad\\SetReconciliation\\setReconciliation.so";
    private static GoClient client;
    public interface SetReconciliation extends Library{
        void Simulate();
        long Add(long a, long b);
    }
    private SetReconciliation setReconciliation;

    private GoClient() {
        setReconciliation = (SetReconciliation) Native.loadLibrary(FILE_PATH, SetReconciliation.class);
    }

    public static GoClient getClient() {
        if (client == null) {
            client = new GoClient();
        }
        return client;
    }

    public void simulate() {
        setReconciliation.Simulate();
    }
    /*public static void main(String[] argv) {
        SetReconciliation setReconciliation = (SetReconciliation) Native.loadLibrary(FILE_PATH, SetReconciliation.class);
        System.out.printf("" +  setReconciliation.Add(5, 7));
    }*/
}
