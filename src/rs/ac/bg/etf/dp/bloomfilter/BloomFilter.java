package rs.ac.bg.etf.dp.bloomfilter;

import java.util.List;

public interface BloomFilter extends Cloneable{
    List<Integer> update(Element e);
    boolean query(Element e);
    BloomFilter clone();
}
