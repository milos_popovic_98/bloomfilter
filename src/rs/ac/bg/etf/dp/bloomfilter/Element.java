package rs.ac.bg.etf.dp.bloomfilter;

public interface Element {
    byte[] getBody();
}
