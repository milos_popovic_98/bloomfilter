package rs.ac.bg.etf.dp.bloomfilter.impl;

import rs.ac.bg.etf.dp.bloomfilter.BloomFilter;
import rs.ac.bg.etf.dp.bloomfilter.Element;
import rs.ac.bg.etf.dp.bloomfilter.HashStrategy;

import java.util.ArrayList;
import java.util.List;

public class DeletableBloomFilter implements BloomFilter {
    private static int ELEM_SIZE = 32;
    private int[] array;
    private List<HashStrategy> strategies;
    private int regionSize;
    private int collisionRegionSize;
    private int capacity;

    public DeletableBloomFilter(int capacity, int collisionRegionSize, List<HashStrategy> strategies) {
        int size = (int) Math.ceil((double) capacity / ELEM_SIZE);

        this.array = new int[size];
        this.collisionRegionSize = collisionRegionSize;
        this.regionSize = (capacity - collisionRegionSize) / collisionRegionSize;
        this.capacity = capacity - collisionRegionSize;
        this.strategies = strategies;
    }


    @Override
    public List<Integer> update(Element e) {
        List<Integer> ret = new ArrayList<>();
        for (HashStrategy hs : strategies) {
            int hashValue = hs.hash(e) % capacity;
            ret.add(hashValue);
            int index = (hashValue + collisionRegionSize) / ELEM_SIZE;
            int offset = (hashValue + collisionRegionSize) % ELEM_SIZE;
            if (isSet(index, offset)) { //bit already set, collision detection
                int region = hashValue / regionSize;
                array[region / ELEM_SIZE] |= 1 << (region % ELEM_SIZE);
            } else {
                array[index] |= 1 << offset;
            }
        }
        return ret;
    }

    @Override
    public boolean query(Element e) {
        for (HashStrategy hs : strategies) {
            int hashValue = hs.hash(e) % capacity;
            if (!queryBit(hashValue))
                return false;
        }
        return true;
    }

    @Override
    public BloomFilter clone() {
        return null;
    }

    public int delete(Element e) {
        if (!query(e)) {
            return -1;
        } else {
            boolean deleted = false;
            for (HashStrategy hs : strategies) {
                int hashValue = hs.hash(e) % capacity;
                int region = hashValue / regionSize;
                if (!isSet(region / ELEM_SIZE, region % ELEM_SIZE)) {
                    int index = (hashValue + collisionRegionSize) / ELEM_SIZE;
                    int offset = (hashValue + collisionRegionSize) % ELEM_SIZE;
                    array[index] |= 1 << offset;
                    deleted = true;
                }
            }
            return deleted ? 1 : 0;
        }
    }
    protected boolean queryBit(int hashValue) {
        int index = (hashValue + collisionRegionSize) / ELEM_SIZE;
        int offset = (hashValue + collisionRegionSize) % ELEM_SIZE;
        return (array[index] & (1 << offset)) > 0;
    }

    protected boolean isSet(int index, int offset) {
        return (array[index] & (1 << offset)) > 0;
    }


}
