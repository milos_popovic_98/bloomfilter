package rs.ac.bg.etf.dp.bloomfilter.impl;

import rs.ac.bg.etf.dp.bloomfilter.Element;
import rs.ac.bg.etf.dp.bloomfilter.HashStrategy;

public class KirschMitzenmacherHashStrategy implements HashStrategy {
    private HashStrategy function1;
    private HashStrategy function2;
    private int k;

    public KirschMitzenmacherHashStrategy(HashStrategy function1, HashStrategy function2, int k) {
        this.function1 = function1;
        this.function2 = function2;
        this.k = k;
    }

    @Override
    public int hash(Element e) {
        return function1.hash(e) + k * function2.hash(e);
    }
}
