package rs.ac.bg.etf.dp.bloomfilter.impl;

import com.google.common.hash.Hashing;
import rs.ac.bg.etf.dp.bloomfilter.BloomFilter;
import rs.ac.bg.etf.dp.bloomfilter.Element;
import rs.ac.bg.etf.dp.bloomfilter.HashStrategy;

import java.util.ArrayList;
import java.util.List;

public class StandardBloomFilter implements BloomFilter {
    private static final int ELEM_SIZE = 32;
    private static HashStrategy[] DEFAULT_HASH_STRATEGY;

    static {
        DEFAULT_HASH_STRATEGY = new HashStrategy[]{
                new SimpleHashStrategy(Hashing.murmur3_32()),
                new SimpleHashStrategy(Hashing.sha256())
        };
    }

    private int[] array;
    private final List<HashStrategy> strategies;
    private int numberOfElements;
    private int filterSize;

    public StandardBloomFilter(int expectedNumberOfElements, int bloomFilterSize, int numberOfHashFunctions) {
        int size = (int) Math.ceil((double) bloomFilterSize / ELEM_SIZE);
        this.array = new int[size];

        this.filterSize = bloomFilterSize;
        double lnVal = Math.log(2);
        numberOfHashFunctions = (numberOfHashFunctions > 0) ? numberOfHashFunctions : (int) Math.ceil(lnVal * bloomFilterSize / expectedNumberOfElements);
        strategies = new ArrayList<>();
        switch (numberOfHashFunctions) {
            case 1:
                strategies.add(DEFAULT_HASH_STRATEGY[0]);
                break;
            case 2:
                strategies.add(DEFAULT_HASH_STRATEGY[0]);
                strategies.add(DEFAULT_HASH_STRATEGY[1]);
                break;
            default:
                strategies.add(DEFAULT_HASH_STRATEGY[0]);
                strategies.add(DEFAULT_HASH_STRATEGY[1]);
                strategies.addAll(generateHashFunctions(numberOfHashFunctions - 2, DEFAULT_HASH_STRATEGY[0], DEFAULT_HASH_STRATEGY[1]));
                break;
        }
    }

    protected List<HashStrategy> generateHashFunctions(int n, HashStrategy hash1, HashStrategy hash2) {
        List<HashStrategy> ret = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            ret.add(new KirschMitzenmacherHashStrategy(hash1, hash2, i + 2));
        }
        return ret;
    }

    public StandardBloomFilter(int bloomFilterSize, List<HashStrategy> strategies) {
        int size = (int) Math.ceil((double) bloomFilterSize / ELEM_SIZE);
        this.filterSize = bloomFilterSize;
        this.array = new int[size]; 
        this.strategies = strategies;
    }


    @Override
    public List<Integer> update(Element e) {
        List<Integer> ret = new ArrayList<>();
        for (HashStrategy hs : strategies) {
            int hashValue = Math.abs(hs.hash(e) % filterSize);
            ret.add(hashValue);
            array[hashValue / ELEM_SIZE] |= 1 << (hashValue % ELEM_SIZE);
        }
        numberOfElements++;
        return ret;
    }

    @Override
    public boolean query(Element e) {
        for (HashStrategy hs : strategies) {
            int hashValue = Math.abs(hs.hash(e) % filterSize);
            if (!queryBit(hashValue)) {
                return false;
            }
        }
        return true;
    }
    public List<Integer> getHash(Element e) {
        List<Integer> ret = new ArrayList<>();
        for (HashStrategy hs : strategies) {
            int hashValue = Math.abs(hs.hash(e) % filterSize);
            ret.add(hashValue);
        }
        return ret;
    }

    protected boolean queryBit(int hashValue) {
        int index = hashValue / ELEM_SIZE;
        int offset = hashValue % ELEM_SIZE;
        return (array[index] & (1 << offset)) != 0;
    }

    @Override
    public StandardBloomFilter clone() {
        StandardBloomFilter copy = new StandardBloomFilter(filterSize, strategies);
        for (int i = 0; i < array.length; i++) {
            copy.array[i] = array[i];
        }
        return copy;
    }
}
