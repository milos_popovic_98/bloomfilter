package rs.ac.bg.etf.dp.bloomfilter.impl;

import rs.ac.bg.etf.dp.bloomfilter.Element;

import java.nio.charset.StandardCharsets;

public class StringElement implements Element {
    private String body;

    public StringElement(String body) {
        this.body = body;
    }

    @Override
    public byte[] getBody() {
        return body.getBytes(StandardCharsets.UTF_8);
    }
}
