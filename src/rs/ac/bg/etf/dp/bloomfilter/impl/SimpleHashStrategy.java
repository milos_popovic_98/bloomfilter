package rs.ac.bg.etf.dp.bloomfilter.impl;

import com.google.common.hash.HashFunction;
import rs.ac.bg.etf.dp.bloomfilter.Element;
import rs.ac.bg.etf.dp.bloomfilter.HashStrategy;

public class SimpleHashStrategy implements HashStrategy {
    private HashFunction function;

    public SimpleHashStrategy(HashFunction function) {
        this.function = function;
    }
    @Override
    public int hash(Element e) {
        return function.hashBytes(e.getBody()).asInt();
    }
}
