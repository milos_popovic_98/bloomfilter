package rs.ac.bg.etf.dp.bloomfilter.impl;

import rs.ac.bg.etf.dp.bloomfilter.BloomFilter;
import rs.ac.bg.etf.dp.bloomfilter.Element;
import rs.ac.bg.etf.dp.bloomfilter.HashStrategy;

import java.util.ArrayList;
import java.util.List;

public class CountingBloomFilter implements BloomFilter {
    private short[] array;
    private final List<HashStrategy> strategies;

    public CountingBloomFilter(int capacity, List<HashStrategy> strategies) {
        this.array = new short[capacity];
        this.strategies = strategies;
    }

    @Override
    public List<Integer> update(Element e) {
        List<Integer> ret = new ArrayList<>();
        for (HashStrategy hs : strategies) {
            int index = Math.abs(hs.hash(e) % array.length);
            ret.add(index);
            array[index]++;
        }
        return ret;
    }

    @Override
    public boolean query(Element e) {
        for (HashStrategy hs : strategies) {
            int index = Math.abs(hs.hash(e) % array.length);
            if (array[index] == 0)
                return false;
        }
        return true;
    }

    public List<Integer> getHash(Element e) {
        List<Integer> ret = new ArrayList<>();
        for (HashStrategy hs : strategies) {
            int index = Math.abs(hs.hash(e) % array.length);
            ret.add(index);
        }
        return ret;
    }

    @Override
    public BloomFilter clone() {
        return null;
    }

    public List<Integer> delete(Element e) {
        if (query(e)) {
            List<Integer> indices = new ArrayList<>();
            for (HashStrategy hs : strategies) {
                int index = Math.abs(hs.hash(e) % array.length);
                indices.add(index);
                array[index]--;
            }
            return indices;
        } else {
            return null;
        }
    }
}
