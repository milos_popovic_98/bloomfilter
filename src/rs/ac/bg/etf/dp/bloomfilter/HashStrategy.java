package rs.ac.bg.etf.dp.bloomfilter;

public interface HashStrategy {
    int hash(Element e);
}
